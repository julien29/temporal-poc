package malt.signup;

import io.temporal.workflow.SignalMethod;
import io.temporal.workflow.WorkflowInterface;
import io.temporal.workflow.WorkflowMethod;

@WorkflowInterface
public interface WorkflowSignup {

    @WorkflowMethod
    void signup(String email);

    @SignalMethod
    void confirmEmailAddress();
}
