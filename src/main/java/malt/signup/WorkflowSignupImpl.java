package malt.signup;

import io.temporal.activity.ActivityOptions;
import io.temporal.common.RetryOptions;
import io.temporal.workflow.Workflow;

import java.time.Duration;

public class WorkflowSignupImpl implements WorkflowSignup {

    boolean emailIsConfirmed = false;

    private final ActivityOptions defaultActivityOptions = ActivityOptions.newBuilder()
            // Timeout options specify when to automatically timeout Activities if the process is taking too long.
            .setStartToCloseTimeout(Duration.ofDays(5))
            // Optionally provide customized RetryOptions.
            // Temporal retries failures by default, this is simply an example.
            .setRetryOptions(RetryOptions.getDefaultInstance())
            .build();
    private final SignupActivities signupSteps = Workflow.newActivityStub(SignupActivities.class, defaultActivityOptions);

    @Override
    public void signup(String email) {

        if (signupSteps.isEmailValid(email)) {
            System.out.println("Wait for email to be confirmed");
            Workflow.await(() -> emailIsConfirmed);
            System.out.println("Email was confirmed");
        } else {
            throw new RuntimeException("Email invalid");
        }

    }

    @Override
    public void confirmEmailAddress() {
        System.out.println("Signal : email confirmed");
        emailIsConfirmed = true;
    }


}
