package malt.signup;

import io.temporal.activity.ActivityInterface;
import io.temporal.activity.ActivityMethod;

@ActivityInterface
public interface SignupActivities {

    @ActivityMethod
    boolean isEmailValid(String email);

    @ActivityMethod
    void sendConfirmationEmail(String email);


}
