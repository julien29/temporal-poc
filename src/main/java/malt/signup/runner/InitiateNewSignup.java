package malt.signup.runner;

import io.temporal.api.common.v1.WorkflowExecution;
import io.temporal.client.WorkflowClient;
import io.temporal.client.WorkflowOptions;
import io.temporal.serviceclient.WorkflowServiceStubs;
import malt.signup.WorkflowSignup;

public class InitiateNewSignup {

    public static void main(String[] args) throws Exception {

        WorkflowClient client = getWorkflowClient();


        String emailAddress = "joe@gmail.com";

        WorkflowOptions options = WorkflowOptions.newBuilder()
                .setTaskQueue("SIGNUP_WORKER")
                // A WorkflowId prevents this it from having duplicate instances, remove it to duplicate.
                .setWorkflowId(emailAddress)
                .build();

        // WorkflowStubs enable calls to methods as if the Workflow object is local, but actually perform an RPC.
        WorkflowSignup workflow = client.newWorkflowStub(WorkflowSignup.class, options);

        // Asynchronous execution. This process will exit after making this call.
        WorkflowExecution we = WorkflowClient.start(workflow::signup, emailAddress);
        System.out.printf("Email: %s WorkflowID: %s RunID: %s%n", emailAddress,we.getWorkflowId(), we.getRunId());

        System.exit(0);
    }

    private static WorkflowClient getWorkflowClient() {
        // WorkflowServiceStubs is a gRPC stubs wrapper that talks to the local Docker instance of the Temporal server.
        WorkflowServiceStubs service = WorkflowServiceStubs.newLocalServiceStubs();
        WorkflowClient client = WorkflowClient.newInstance(service);
        return client;
    }

}

