package malt.signup.runner;

import io.temporal.client.WorkflowClient;
import io.temporal.serviceclient.WorkflowServiceStubs;
import malt.signup.WorkflowSignup;

public class ExternalSignalConfirmEmail {

    public static void main(String[] args) throws Exception {


        WorkflowClient client = getWorkflowClient();

        String emailAddress = "joe@gmail.com";
        client.newWorkflowStub(WorkflowSignup.class, emailAddress).confirmEmailAddress();

        System.exit(0);
    }

    private static WorkflowClient getWorkflowClient() {
        // WorkflowServiceStubs is a gRPC stubs wrapper that talks to the local Docker instance of the Temporal server.
        WorkflowServiceStubs service = WorkflowServiceStubs.newLocalServiceStubs();
        return WorkflowClient.newInstance(service);
    }
}

